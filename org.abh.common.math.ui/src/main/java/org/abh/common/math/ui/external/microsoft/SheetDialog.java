/**
 * Copyright (C) 2016, Antony Holmes
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *  1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *  3. Neither the name of copyright holder nor the names of its contributors 
 *     may be used to endorse or promote products derived from this software 
 *     without specific prior written permission. 
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.abh.common.math.ui.external.microsoft;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

import org.abh.common.math.external.microsoft.Excel;
import org.abh.common.ui.UI;
import org.abh.common.ui.combobox.ModernComboBox;
import org.abh.common.ui.dialog.ModernDialogTaskWindow;
import org.abh.common.ui.panel.MatrixPanel;
import org.abh.common.ui.text.ModernAutoSizeLabel;
import org.abh.common.ui.widget.ModernWidget;
import org.abh.common.ui.window.ModernWindow;



// TODO: Auto-generated Javadoc
/**
 * The class SheetDialog.
 */
public class SheetDialog extends ModernDialogTaskWindow {
	
	/**
	 * The constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * The member sheet combo.
	 */
	private ModernComboBox mSheetCombo = new ModernComboBox();

	
	/**
	 * Instantiates a new sheet dialog.
	 *
	 * @param parent the parent
	 * @param file the file
	 */
	public SheetDialog(ModernWindow parent, Path file) {
		super(parent);
		
		setTitle("Select Sheet");

		try {
			List<String> names = Excel.getSheetNames(file);
			
			for (String name : names) {
				mSheetCombo.addMenuItem(name);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		mSheetCombo.setSelectedIndex(0);

		
		int[] rows = {ModernWidget.WIDGET_HEIGHT};
		int[] columns = {100, 400, ModernWidget.WIDGET_HEIGHT};
		
		MatrixPanel panel = new MatrixPanel(rows, columns, ModernWidget.PADDING, ModernWidget.PADDING);

		panel.add(new ModernAutoSizeLabel("Sheet"));
		panel.add(mSheetCombo);
		
		
		setContent(panel);


		setSize(320, 240);
		
		UI.centerWindowToScreen(this);
	}

	/**
	 * Gets the sheet.
	 *
	 * @return the sheet
	 */
	public String getSheet() {
		return mSheetCombo.getText();
	}
}
